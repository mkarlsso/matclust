    
function out = stimFilter()


global clustdata;
global clustattrib;


a = inputdlg('Enter time window after stim (secs)');
w = str2num(a{1});

out = ((clustdata.params(:,7)>=0) & (clustdata.params(:,7)<w));

